# parsind edgar

## aim

I want to extract the share hold form the edgar filings for a list of filings provided in a excel file

## code

The code has a class to read in the excel file, one to download and manage the offline files and one to parse a given file.
The code itself is rather simple, it simply searches for the default spelling of my target and then searches for number in that or the next line.
It is rather effective.
