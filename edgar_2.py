#!/usr/bin/python3

import sys
try:
    assert sys.version_info >= (3, 6)
except AssertionError:
    print('You need to run this with Python 3.6 or higher!')
    sys.exit(1)
import urllib.request
import csv
import time
from random import randint
import os.path
import bs4
import logging

logging.basicConfig(filename='edgar_2.log', filemode='w')
logger = logging.getLogger()

# BASELINK = "https://sec.gov/Archives/"
BASELINK = 'file:///home/f-szlago/Edgar/e3/'
# INFILE_PATH = "./Data_Table_13G_missing_Lion_output.csv"
# OUTFILE_PATH = "./Data_Table_13G_missing_Lion_output_run2.csv"
INFILE_PATH = './data_out.csv'
OUTFILE_PATH = './data_out_2.csv'


def count_occurences(word, lines):
    """"Counts the occurencies of a word
    in a given string
    """
    n = 0
    for line in lines:
        if word in line:
            n = n + 1
    return n


def clean_string(word):
    """Removes unnecesary stuff from a string"""
    if ')' in word:
        word = word.split(')')[1]
    if ':' in word:
        word = word.split(':')[1]
    if '-' in word:
        word = word.replace('-', '')
    if 'none' in word:
        word = word.replace('none', '')
    if '+' in word:
        word = word.replace('+', '')
    if '*' in word:
        word = word.replace('*', '')
    if '_' in word:
        word = word.replace('_', '')
    if ',' in word:
        word = word.replace(',', '.')
    return word.strip()


class Reader:
    """Read in the excel file and write the outfile"""

    def __init__(self, clean):
        self.input_file = open(INFILE_PATH, 'r', newline='')
        if clean:
            self.output_file = open(OUTFILE_PATH, 'w', newline='')
            self.output_file.close
            # I hope that closing the file before use delets the old content.
            self.output_file = open(OUTFILE_PATH, 'w', newline='')
        else:
            self.output_file = open(OUTFILE_PATH, 'a', newline='')
        self.reader = csv.reader(self.input_file, delimiter=',')
        self.writer = csv.writer(self.output_file, delimiter=',')

        if clean:
            self.writer.writerow(next(self.reader))
        else:
            next(self.reader)  # omit the header line

    def __del__(self):
        self.input_file.close()
        self.output_file.close()

    def next(self):
        """give the next value"""
        tmp = next(self.reader)
        link = tmp[6]
        return (link, tmp)

    def _write(self, data):
        """write data to the excel file"""
        self.writer.writerow(data)

    def jump(self, n):
        """skip the next n rows of the file"""
        if n <= 0:
            return
        for i in range(0,n):
            next(self.reader)

    def write_add_mon_var(self, case_id, local, share, mon_var, data):
        """writes a result including monitoring variables to file"""
        mon_var[2] = share
        self._write([case_id, local ] + mon_var + data)

    def write_2(self, data, local, share):
        """even newer writing function, scipping mon. vars."""
        if local is not None:
            data[1] = local
        if share is not None:
            data[2] = share
        self._write(data)

class Downloader:
    """This class manages the downloading and offline copies of the data
    For significantly higher performance I dont want to download everything
    for every run. Therefor I keep local copies
    """

    def txt_web(self, link):
        # this failes for a large amount.
        # Namely all companies that do not have a filing.
        link = link.replace('index.htm', '/filing.txt')
        link = link.replace('-', '')
        return link

    def txt_local(self, link):
        # this is indicated by mon_var local == 1
        link = link.replace('index.htm', '/filing.txt')
        link = link.replace('-','')
        link = '-'.join(link.rsplit('/',2))
        return link

    def download_sec(self, link, save):
        # I will directly acess the txt file
        if 'file:///' in BASELINK:
            if os.path.isfile('./' + self.txt_local(link)):
                print('link')
                return self.txt_local(link)
            else:
                return None
        link_web = self.txt_web(link)
        try:
            filing = urllib.request.urlopen(BASELINK + link_web).read()
        except Exception:
            return None
        if save:
            with open('./' + self.txt_local(link), 'wb') as outf:
                outf.write(filing)
        return self.txt_local(link)

    def download_sec_2(self, link, save=True):
        # the files, that cannot be downloaded in just one step
        if 'file:///' in BASELINK:
            return None
        try:
            website = urllib.request.urlopen(BASELINK + link).read()
            website_soup = bs4.BeautifulSoup(website,features='lxml')
        except Exception as err:
            logger.error('failed website: ' + link + str(err))
            return None
        try:
            table = website_soup.table
        except Exception:
            logger.error('no table')

        if not 'Complete' in table.get_text():
            logger.warning('no correct in website')
            return None
        correct_row = None
        for row in table.find_all('tr'):
            if 'Complete' in row.get_text():
                correct_row = row
        if correct_row is None:
            logger.critical('no correct row despite correct table')
            return None # should never happen
        try:
            href = correct_row.find('a').get('href')
        except Exception:
            logger.error('no href in correct row')
            return None

        # download the actual filing after strip leading '/Archives/'
        if '/Archives/' in href:
            href = href.replace('/Archives/','')
        try:
            filing = urllib.request.urlopen(BASELINK + href).read()
        except Exception as err:
            logger.error('failed retrieving filing: ' + str(err))
            return None
        if save:
            href_local = '-'.join(href.rsplit('/',2))
            with open('./' + href_local, 'wb') as outfile:
                outfile.write(filing)
            return href_local # this return value is actually important!
        return None


class Parser:

    debug = False

    def parse(self, file):
        # for now the simplest possible parse by exact matching
        share = self._parse_run_2(file)
        return share

    def _parse_exact_matching(self, path):
        correct_block = False
        with open(path,'r') as ifile:
            for line in ifile:
                if 'Percent of Class:' in line:
                    if self.debug:
                        print('Percent of Class found')
                    percent = line.split(':')[1].strip()
                    if '%' in percent:
                        percent = percent.replace('%','')
                    if '*' in percent:
                        percent = percent.replace('*','')
                    try:
                        return float(percent)
                    except ValueError:
                        return None

            for line in ifile: # really no longer needed...
                if 'Item 4' in line and 'Ownership' in line:
                    correct_block = True
                    if self.debug:
                        print('correct_block_found')
                if correct_block == False:
                    continue
                if 'Percent of Class:' in line:
                    percent = line.split(':')[1].strip()
                    if '%' in percent:
                        percent = percent.replace('%','')
                    if '*' in percent:
                        percent = percent.replace('*','')
                    try:
                        return float(percent)
                    except ValueError as err:
                        return None
                if 'Item 5' in line:
                    correct_block = False
            return None

    def _parse_run_2(self, path):
        '''' legend: 
            -1 - multiple companies / no clear way of extracting the data
            1 - percent of class:, though new line
            2 - html, percent of class:, table
            3 - html, Percent of class, text
        '''
        # I'll first convert everything to a string, all lower case. Then check if the string contains exactly one 'percent of class:' if so, extract percent.
        # if not I can search for 'percent of class' and do the same. 
        infile = open(path, 'r')
        data = infile.read()
        infile.close()
        data = data.lower()

        if '<html>' in data:
            html = True
        else:
            html = False

        # html to normal text:
        if html:
            soup = bs4.BeautifulSoup(data.split('<html>')[1].split('</html>')[0], features='lxml')
            text = soup.get_text()
        else:
            text = data

        lines = []
        for i in text.split('\n'):
            if i == '':
                continue
            lines.append(i)

        # first attempt: check for 'percent of class:' and the next line
        poc = count_occurences('percent of class:', lines)
        if poc == 1:
            for i in range(0, len(lines) - 2):
                if 'percent of class:' in lines[i]:
                    if '%' in lines[i] and not '%' in lines[i + 1]:
                        try:
                            left_part = lines[i].split('percent of class:')[1].split('%')[0]
                            #if 'none' in left_part:
                                #left_part = left_part.replace('none','')
                            tmp = float(clean_string(left_part))
                            return tmp
                        except Exception as err:
                            logger.error('failed splitting in line with "percent of class:" and "%": ' + str(err))
                    if '%' in lines[i + 1] and not '%' in lines[i + 2]:
                        try:
                            tmp = float(lines[i + 1].split('%')[0])
                            return tmp
                        except Exception as err:
                            logger.error('failed splitting in line after "percent of class:": ' + str(err))

        poc = count_occurences('percent of class represented by amount in row', lines)
        if poc == 1:
            for i in range(0, len(lines) - 2):
                if 'percent of class represented by amount in row' in lines[i]:
                    if '%' in lines[i] and not '%' in lines[i + 1]:
                        try:
                            left_part = lines[i].split('percent of class represented by amount in row')[1].split('%')[0] 
                            tmp = float(clean_string(left_part))
                            return tmp
                        except Exception as err:
                            logger.error('failed splitting in line with "percent of class represented by amount in row" and "%": ' + str(err))
                    if '%' in lines[i + 1] and not '%' in lines[i + 2]:
                        try:
                            tmp = float(lines[i + 1].split('%')[0])
                            return tmp
                        except Exception as err:
                            logger.error('failed splitting in line after "percent of class represented by amount in row": ' + str(err))


def __main__():

    start_id = 0
    reader = Reader(True if start_id == 0 else False)
    downloader = Downloader()
    parser = Parser()

    try:
        for i in range(0,30577):
            #time.sleep(randint(2,4))
            tmp_mon_var = []
            (link, data) = reader.next()
            if int(data[0]) < start_id:
                continue
            if data[1] == '1':
                reader.write_2(data, None, None)
                continue
            elif 'edgar' in str(data[1]):
                # Ich musste für die dritte version data[2] benutzen statt data[3], da ich eine Variable nicht benutze
                if data[2] != '':
                    reader.write_2(data, None, None)
                    continue
                path = str(data[1])
            else:
                # for now I only want to use the already local copies
                # reader.write_2(data, None, None)
                # continue
                # this line downloads the new file
                path = downloader.download_sec_2(link, True)
            if path is None:
                reader.write_2(data, None, None)
                continue
            tmp = parser.parse(path)
            if tmp is None:
                reader.write_2(data, path, None)
            else:
                reader.write_2(data, path, tmp)
                print(str(tmp))
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    __main__()
